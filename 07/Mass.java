class Mass {
    public static void main(String[] args) {
        int n_e = 5;
        int[] a = {1, 4, 3, 2, 0};
        int n_s = 0;
        while (n_s != n_e - 1) {
            n_s = 0;
            int t;
            for (int i = 0; i < n_e - 1; i++) {
                if (a[i] > a[i + 1]) {
                    t = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = t;
                } else {
                    n_s++;
                }
            }
        }
        for (int i = 0; i < n_e; i++) {
            System.out.print(a[i] + " ");
        }
    }
}

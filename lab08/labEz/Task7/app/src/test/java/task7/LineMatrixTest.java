package task7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LineMatrixTest {
    @Test
    void matrixToLine() {
        int[][] a = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        assertEquals("1 2 3 4 5 6 7 8 9 ", LineMatrix.toLine(a));
    }

    @Test
    void matrixToLineOne() {
        int[][] a = {{1}};
        assertEquals("1 ", LineMatrix.toLine(a));
    }

    @Test
    void matrixToLineBlank() {
        int[][] a = {{}};
        assertEquals("", LineMatrix.toLine(a));
    }
}

package task7;

public class LineMatrix {
    static String toLine(int[][] matrix) {
        String b = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                b = b + matrix[i][j] + " ";
            }
        }
        return b;
    }
}


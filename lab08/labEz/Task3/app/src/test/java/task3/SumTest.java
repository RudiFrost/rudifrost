package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumTest {
    @Test
    void checkSumTen() {
        int a = 10;
        int b = 55;
        assertEquals(b, SumN.summ(a));
    }

    @Test
    void checkSumZero() {
        int a = 0;
        int b = 0;
        assertEquals(b, SumN.summ(a));
    }
}


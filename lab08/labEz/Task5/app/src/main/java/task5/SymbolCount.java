package task5;

class SymbolCount {
    static int count(String str, char symbol) {
        char[] array_str = str.toCharArray();
        int sum = 0;
        for (int i = 0; i < array_str.length; i++) {
            if (array_str[i] == symbol) {
                sum++;
            }
        }
        return sum;
    }
}

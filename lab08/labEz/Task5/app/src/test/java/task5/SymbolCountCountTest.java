package task5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SymbolCountCountTest {
    @Test
    void symbol() {
        String str = "aaaaaabbbbcc";
        char symbol = 'a';
        assertEquals(6, SymbolCount.count(str, symbol));
    }

    @Test
    void symbolOne() {
        String str = "a";
        char symbol = 'a';
        assertEquals(1, SymbolCount.count(str, symbol));
    }

    @Test
    void symbolBlank() {
        String str = "";
        char symbol = 'a';
        assertEquals(0, SymbolCount.count(str, symbol));
    }

    @Test
    void symbolInLineZero() {
        String str = "zzzxxxccc";
        char symbol = 'a';
        assertEquals(0, SymbolCount.count(str, symbol));
    }
}

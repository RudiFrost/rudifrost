package task1;

import java.util.*;
import java.lang.*;


class ReFib {
    public static long fib(int a, long[] fibbers){
        if (fibbers[a - 1] != 0){
            return fibbers[a - 1];
        }
        if (a <= 2){
            fibbers[a - 1] = a - 1;
            return fibbers[a - 1];
        }
        long result = fib(a - 1, fibbers) + fib(a - 2, fibbers);
        fibbers[a - 1] = result;
        return result;
    }

    public static long[] getFib(int n){
        if (n == 0){
            return null;
        }
        long[] fibbers_new = new long[n];
        fib(n, fibbers_new);
        return fibbers_new;
    }

    public static void main(String[] args){
        int n = 20;
        System.out.println(Arrays.toString(getFib(n)));
    }
}



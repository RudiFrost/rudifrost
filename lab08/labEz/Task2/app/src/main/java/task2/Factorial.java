package task2;

class Factorial {
    static long fact(int max){
        long a = 1;
        for (int i = 1; i <= max; i++) {
            a = a * i;
        }

        return a;
    }
}

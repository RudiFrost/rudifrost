package task2;

class ReFactorial {
    static long fact(int num) {
        return factorial(num);

    }

    public static long factorial(long a) {
        if ((a == 1) || (a == 0)) {
            return 1;
        } else {
            return a * factorial(a - 1);
        }
    }
}

package task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {
    @Test
    void checkFib() {
        int a = 5;
        int b = 120;
        assertEquals(b, Factorial.fact(a));
    }
    @Test
    void checkOneFib() {
        int a = 1;
        int b = 1;
        assertEquals(b, Factorial.fact(a));
    }
    @Test
    void checkZeroFib() {
        int a = 0;
        int b = 1;
        assertEquals(b, ReFactorial.fact(a));
    }
}

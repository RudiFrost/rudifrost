package task5;

import java.util.Scanner;

import static java.lang.Math.abs;

class Shifting {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int range = sc.nextInt();
        int shift = sc.nextInt();

        Segment[] segments = new Segment[n];

        int max = shift + range;
        int min = abs(range - shift);
        int crossings = 0;

        for (int i = 0; i < n; i++) {
            segments[i] = new Segment();
            segments[i].point11 = (int) (Math.random() * max) + min;
            segments[i].point12 = (int) (Math.random() * max) + min;
            segments[i].point21 = (int) (Math.random() * max) + min;
            segments[i].point22 = (int) (Math.random() * max) + min;
        }

        for (int i = 0; i < n; i++) {
            if (((segments[i].point11 > segments[i].point21 && segments[i].point11 < segments[i].point22) ||
                    (segments[i].point12 > segments[i].point21 && segments[i].point12 < segments[i].point22)) ||
                    (segments[i].point11 < segments[i].point21 && segments[i].point12 > segments[i].point22)) {
                crossings++;

            }
        }
        System.out.println(crossings);

    }
}

package task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class IntToStringTest {
    @Test
    void toStringTest() {
        IntToString st = new IntToString();
        String b = "123";
        assertEquals(b, st.tostr("+123"));
    }

    @Test
    void BlankTest() {
        IntToString st = new IntToString();
        String b = "";
        assertEquals(b, st.tostr(""));
    }

    @Test
    void negativeTest() {
        IntToString st = new IntToString();
        String b = "-123";
        assertEquals(b, st.tostr("-123"));
    }
}

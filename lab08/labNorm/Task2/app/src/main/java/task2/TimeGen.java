package task2;

class TimeGen {
    long timeout;
    double[] massive;
    int n;
    long spentTime;

    TimeGen(long timeout, int n) {
        this.timeout = timeout;
        this.n = n;
    }

    boolean success() {
        return spentTime < timeout;
    }

    public double[] getMassive() {
        if (success()) {
            return this.massive;
        }
        return null;
    }

    void generate() {
        massive = new double[n];
        long time = System.currentTimeMillis();
        for (int i = 0; i < n; i++) {
            massive[i] = Math.random();
        }
        this.spentTime = System.currentTimeMillis() - time;
    }
}

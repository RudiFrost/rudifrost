package task6;

import java.util.Scanner;

class Matrix {
    public static int[][][] setMatrix(int s, int n) {
        int[][][] matrix = new int[s][n][n];
        for (int d = 0; d < s; d++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[d][i][j] = (int) (Math.random() * 10);
                }
            }
        }
        return matrix;
    }


    public static int[][] multiplyMatrix(int[][] matrix1, int[][] matrix2) {

        int[][] result = new int[matrix1.length][matrix2.length];
        for (int i = 0; i < matrix1[0].length; i++) {
            for (int j = 0; j < matrix2.length; j++) {
                for (int y = 0; y < matrix2[0].length; y++) {
                    result[i][j] = result[i][j] + matrix1[i][y] * matrix2[y][j];
                }
            }
        }
        return result;
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        int width = sc.nextInt();
        long timeStart = System.currentTimeMillis();
        int[][][] st = setMatrix(count, width);
        for (int i = 0; i < count - 1; i++) {
            st[i + 1] = multiplyMatrix(st[i], st[i + 1]);
        }
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(st[st.length - 1][i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(System.currentTimeMillis() - timeStart + "мс");

    }
}


package task7;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {

        Node nodeFactorial = new Node(2, new FactorialAction());
        Node nodePower = new Node(1, new PowerAction());
        Node nodeFibonacci = new Node(1, new FibonacciAction());
        Node nodeHello = new Node(0, new HelloAction());
        Node nodeGoodbye = new Node(0, new GoodbyeAction());
        nodeFactorial.children[0] = nodePower;
        nodeFactorial.children[1] = nodeFibonacci;
        nodePower.setParent(nodeFactorial);
        nodeFibonacci.setParent(nodeFactorial);
        nodePower.children[0] = nodeHello;
        nodeFibonacci.children[0] = nodeGoodbye;
        nodeHello.setParent(nodePower);
        nodeGoodbye.setParent(nodeFibonacci);
        Node node = nodeFactorial;
        char emptyActionChar = '-';
        char a = emptyActionChar;
        char b = emptyActionChar;
        while (!(a == '0' && !node.hasParent())) {
            if (!node.hasParent())
                System.out.println("Нажмите 0, чтобы выйти" + "\n" + "Нажмите 1, чтобы " + node.action.description);
            else
                System.out.println(node.getQuestion());
            for (int i = 0; i < node.getChildren().length; i++) {
                if (!node.hasChildren()) {
                    System.out.println("Нажмите " + (i + 2) + ", чтобы " + node.getChildren()[i].getAction().getDescription());
                } else {
                    System.out.println("Нажмите " + (i + 2) + ", чтобы перейти дальше");
                }
            }
            Scanner sc = new Scanner(System.in);
            a = sc.nextLine().charAt(0);
            b = sc.nextLine().charAt(1);
            if (a == '1') {
                try {
                    if (node.getAction().args == 1) {
                        int n = sc.nextInt();
                        node.doAction(n);
                    } else {
                        node.doAction(0);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(e);
                }

            } else if (a == '0') {
                if (node.hasParent()) {
                    node = node.goToParent();
                    a = emptyActionChar;
                }
            } else if (a > '1' && a <= node.children.length) {
                if (b != emptyActionChar){
                    node = node.goToChild(a + b - 100);
                }
                node = node.goToChild(a - 50);
            }
        }
    }
}



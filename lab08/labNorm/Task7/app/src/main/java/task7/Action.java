package task7;

public abstract class Action {
    public String description;
    public int args = 1;

    public abstract void doSmth(int a);

    public abstract String getDescription();

}

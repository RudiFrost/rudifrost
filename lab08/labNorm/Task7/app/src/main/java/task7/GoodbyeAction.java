package task7;

public class GoodbyeAction extends Action {
    public GoodbyeAction() {
        this.description = "Вывести прощание";
        this.args = 0;
    }

    @Override
    public void doSmth(int a) {
        System.out.println("Прощай");
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}

package task7;

public class PowerAction extends Action {
    public PowerAction() {
        this.description = "вывести квадрат числа";
    }

    @Override
    public void doSmth(int a) {
        System.out.println(a + " в квадрате равно: " + Math.pow(a, 2));
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}

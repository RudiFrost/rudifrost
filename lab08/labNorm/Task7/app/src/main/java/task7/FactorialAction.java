package task7;

public class FactorialAction extends Action {
    public FactorialAction() {
        this.description = "вывести факториал числа";
    }

    @Override
    public void doSmth(int a) {
        long fact = 1;
        for (int i = 1; i <= a; i++) {
            fact = fact * i;
        }
        System.out.println("Факториал числа "+ a + " равен: " + fact);
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}

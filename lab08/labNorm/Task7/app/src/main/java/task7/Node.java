package task7;

public class Node {
    public String question;
    public Action action;
    public Node parent;
    public Node[] children;

    public Node(int n, Action action) {
        this.children = new Node[n];
        this.action = action;
        this.question = "Нажмите 0, чтобы поднятся выше" + "\n";
        this.question += "Нажмите 1, чтобы " + action.description;
    }

    public String getQuestion() {
        return this.question;
    }

    public Node getParent() {
        return this.parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node[] getChildren() {
        return this.children;
    }

    public Action getAction() {
        return this.action;
    }

    public boolean hasNext() {
        return this.children.length != 0;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean hasChildren() {
        return children != null;
    }

    public Node goToChild(int n) throws Exception {
        if (n >= children.length) {
            throw new Exception("Вводите числа которые дают на выбор");
        }
        return this.children[n];
    }

    public Node goToParent() throws Exception {
        if (this.parent == null) {
            throw new Exception("Вводите числа которые дают на выбор");
        }
        return this.parent;
    }

    public void doAction(int a) {
        this.action.doSmth(a);
    }
}


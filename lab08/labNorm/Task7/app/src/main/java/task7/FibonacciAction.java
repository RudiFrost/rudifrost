package task7;

public class FibonacciAction extends Action {
    public FibonacciAction() {
        this.description = "вывести числа фибоначчи";
    }

    @Override
    public void doSmth(int a) {
        long first = 0;
        long second = 1;
        long ending = a;
        long[] arr = new long[a];
        if (ending == 1) {
            arr[0] = first;
        } else if (ending > 1) {
            arr[0] = first;
            arr[1] = second;
        }
        for (int i = 0; i < ending - 2; i++) {
            long result = first + second;
            arr[i + 2] = result;
            first = second;
            second = result;
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }


    @Override
    public String getDescription() {
        return this.description;
    }
}

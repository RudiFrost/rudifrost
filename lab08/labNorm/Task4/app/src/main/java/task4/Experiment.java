package task4;

public class Experiment {


    OneTry getTry(double numberThreshold, double coefficient, int maxTime) {
        OneTry finalTry = new OneTry();

        double max = coefficient * numberThreshold;
        double min = -max;

        long timeStart = System.currentTimeMillis();
        while (maxTime > System.currentTimeMillis() - timeStart) {
            try {
                Thread.sleep((int) Math.random() * (100) + 900);
                double randomNumber = (Math.random() * ((max - min) + 1)) + min;
                if (randomNumber < numberThreshold) {
                    finalTry.time = System.currentTimeMillis() - timeStart;
                    finalTry.number = randomNumber;
                } else {
                    break;
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
        return finalTry;
    }
}

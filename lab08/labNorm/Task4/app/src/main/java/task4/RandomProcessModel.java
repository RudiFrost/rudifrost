package task4;

import java.util.Scanner;

public class RandomProcessModel {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double numberThreshold = sc.nextDouble();
        double coefficient = sc.nextDouble();
        int max_time = sc.nextInt();

        Experiment exp = new Experiment();
        OneTry ot = exp.getTry(numberThreshold, coefficient, max_time);
        System.out.println(ot.time + ": " + ot.number);

    }
}


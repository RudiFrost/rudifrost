package task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TriangleTest {
    @Test
    void perimeterTriangle() throws NotTriangleException {
        int[] lenRib4 = new int[]{1, 2, 2};
        Triangle a = new Triangle(lenRib4);
        assertEquals(5, a.perimeterPolygon());
    }

    @Test
    void toStringTriangle() throws NotTriangleException {
        int[] lenRib4 = new int[]{1, 2, 3};
        Triangle a = new Triangle(lenRib4);
        assertEquals("Треугольник периметр которого равен 6", a.output());
    }

    @Test
    void triangleException() throws NotTriangleException {
        int[] lenRib4 = new int[]{1, 2};
        NotTriangleException exception = assertThrows(NotTriangleException.class, () -> new Triangle(lenRib4));
        assertEquals("Не треугольник", exception.getMessage());
    }

}



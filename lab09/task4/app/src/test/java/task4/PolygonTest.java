package task4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PolygonTest {
    @Test
    void perimeterPolygon() {
        int[] lenRib4 = new int[]{1, 2, 2, 2, 2};
        Polygon a = new Polygon(lenRib4);
        assertEquals(9, a.perimeterPolygon());
    }

    @Test
    void toStringPolygon() {
        int[] lenRib4 = new int[]{1, 2, 2, 2, 2};
        Polygon a = new Polygon(lenRib4);
        assertEquals("5-угольник периметр которого равен 9", a.output());
    }

    @Test
    void triangleCheckFalse() {
        int[] lenRib4 = new int[]{1, 2, 100};
        Polygon a = new Polygon(lenRib4);
        assertFalse(a.checkLengthCorrect());
    }

    @Test
    void triangleCheckTrue() {
        int[] lenRib4 = new int[]{1, 2, 3};
        Polygon a = new Polygon(lenRib4);
        assertTrue(a.checkLengthCorrect());
    }
}

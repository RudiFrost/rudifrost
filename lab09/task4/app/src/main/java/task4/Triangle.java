package task4;

class Triangle extends Polygon {

    Triangle(int[] edges) throws NotTriangleException {
        super(edges);
        if (edges.length != 3) {
            edges = null;
            perimeter = 0;
            throw new NotTriangleException("Не треугольник");
        }
    }

    public String output() {
        if (perimeter == 0) {
            return "Этот треугольник не был треугольником...";
        }
        return "Треугольник периметр которого равен " + this.perimeter;
    }
}

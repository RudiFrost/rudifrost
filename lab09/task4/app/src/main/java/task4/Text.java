package task4;

public class Text implements GeneralInterface {
    String text;

    Text(String text) {
        this.text = text;
    }

    public String output() {
        return this.text;
    }
}

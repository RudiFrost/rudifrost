package task4;

public class NotTriangleException extends Exception {

    String message;

    public NotTriangleException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

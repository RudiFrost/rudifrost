package task4;

class Operations {
    public GeneralInterface[] allPolygons;

    Operations(GeneralInterface[] allPolygons) {
        this.allPolygons = allPolygons;
    }

    public static void main(String[] args) throws NotTriangleException {

        int[] edges1 = new int[]{1, 1, 1};
        Triangle triangle1 = new Triangle(edges1);

        int[] edges2 = new int[]{1, 2, 3};
        Triangle triangle2 = new Triangle(edges2);

        int[] edges3 = new int[]{1, 2, 3, 4, 5};
        Polygon polygon = new Polygon(edges3);

        Text text1 = new Text("да");
        Text text2 = new Text("нет");

        GeneralInterface[] in = new GeneralInterface[]{triangle1, triangle2, polygon, text1, text2};
        Operations op = new Operations(in);
        op.printAllObjects();
    }

    public void printAllObjects() {
        for (int i = 0; i < this.allPolygons.length; i++) {
            System.out.println(this.allPolygons[i].output());
        }
    }
}


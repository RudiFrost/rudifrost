package task4;

public class Polygon implements GeneralInterface {
    protected int perimeter;
    protected int[] edges;

    Polygon(int[] edges) {
        this.edges = edges;
        this.perimeter = perimeterPolygon();
        this.checkLengthCorrect();

    }

    Polygon(){

    }

    public boolean checkLengthCorrect() {
        int sum = this.perimeter;
        for (int i = 0; i < this.edges.length; i++) {
            if (2 * this.edges[i] > sum) {
                return false;
            }
        }
        return true;
    }


    public int perimeterPolygon() {
        int perimeter = 0;
        for (int i = 0; i < this.edges.length; i++) {
            perimeter += this.edges[i];
        }
        return perimeter;
    }

    public String output() {
        return this.edges.length + "-угольник периметр которого равен " + this.perimeter;
    }
}

package task3;

public class Engineer extends Employee {

    Engineer(String name, int money) {
        super(name, money);
    }

    public String engineer() {
        return "Я инженерю";
    }

    public String toString() {
        return "Имя = " + name + ", должность = инженер" + ", зарплата = " + salary;
    }


}

package task3;

class Main {
    public static void main(String[] args) {
        Employee[] employee = new Employee[4];

        employee[0] = new Accountant("Галина", 20000);
        employee[1] = new ChiefAccountant("Людмила", 30000);
        employee[2] = new Engineer("Анатолий", 25000);
        employee[3] = new Worker("Геннадий", 10000);
        for (int i = 0; i < employee.length; i++) {
            System.out.println(employee[i]);
        }
    }

}

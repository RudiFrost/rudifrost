package task3;

public class ChiefAccountant extends Accountant {

    ChiefAccountant(String name, int money) {
        super(name, money);
    }

    public String accountant() {
        return "Я бухгалтерю и меняю зарплаты";
    }

    public void changeSalary(Employee employer, int money) {
        employer.salary = money;
    }

    public String toString() {
        return "Имя = " + name + ", должность = главный бухгалтер" + ", зарплата = " + salary;
    }

}

package task3;

public class Employee {
    protected String name;
    protected int salary;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Имя = " + name + ", должность = сотрудник" + ", зарплата = " + salary;
    }

    public int hashCode() {
        return name.length() * 100 + salary;
    }

    public boolean equals(Object o) {
        if (o.getClass() == this.getClass()) {
            return this.name == ((Employee) o).getName() &&
                    this.salary == ((Employee) o).getSalary();
        }
        return false;
    }

    public Object clone(){
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}

package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChiefAccountantTest {
    @Test
    void chiefAccountant() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Людмила", 30000);
        assertEquals("Я бухгалтерю и меняю зарплаты", chiefAccountant.accountant());
    }

    @Test
    void chiefAccountantToString() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Людмила", 30000);
        assertEquals("Имя = Людмила, должность = главный бухгалтер, зарплата = 30000", chiefAccountant.toString());
    }

    @Test
    void chiefAccountantChangeSalary() {
        ChiefAccountant chiefAccountant = new ChiefAccountant("Людмила", 30000);
        Worker worker = new Worker("Геннадий", 10000);
        chiefAccountant.changeSalary(worker, 5000);
        assertEquals(5000, worker.getSalary());
    }
}

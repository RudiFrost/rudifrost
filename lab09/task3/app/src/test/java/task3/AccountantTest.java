package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountantTest {
    @Test
    void accountant() {
        Accountant accountant = new Accountant("Галина", 20000);
        assertEquals("Я бухгалтерю", accountant.accountant());
    }

    @Test
    void accountantToString() {
        Accountant accountant = new Accountant("Галина", 20000);
        assertEquals("Имя = Галина, должность = бухгалтер, зарплата = 20000", accountant.toString());
    }
}

package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {
    @Test
    void equalsFalse() {
        Worker worker = new Worker("Геннадий", 10000);
        Engineer engineer = new Engineer("Анатолий", 25000);
        assertFalse(worker.equals(engineer));
    }

    @Test
    void equalsTrue() {
        Worker worker = new Worker("Геннадий", 10000);
        Worker worker1 = new Worker("Геннадий", 10000);
        assertTrue(worker.equals(worker1));
    }

    @Test
    void hashCodeCheck() {
        Employee employee = new Employee("Геннадий", 10000);
        assertEquals(10800, employee.hashCode());
    }

    @Test
    void cloneCheck() {
        Employee employee = new Employee("Геннадий", 10000);
        assertNull(employee.clone());
    }

    @Test
    void SalaryCheck() {
        Employee employee = new Employee("Геннадий", 10000);
        assertEquals(10000, employee.getSalary());
    }
    @Test
    void NameCheck() {
        Employee employee = new Employee("Геннадий", 10000);
        assertEquals("Геннадий", employee.getName());
    }
    @Test
    void employeeToString() {
        Employee employee = new Employee("Геннадий", 10000);
        assertEquals("Имя = Геннадий, должность = сотрудник, зарплата = 10000", employee.toString());
    }
}

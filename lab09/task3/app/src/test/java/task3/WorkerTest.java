package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WorkerTest {
    @Test
    void worker() {
        Worker worker = new Worker("Геннадий", 10000);
        assertEquals("Я работаю", worker.worker());
    }

    @Test
    void workerToString() {
        Worker worker = new Worker("Геннадий", 10000);
        assertEquals("Имя = Геннадий, должность = рабочий, зарплата = 10000", worker.toString());
    }
}

package task3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EngineerTest {
    @Test
    void engineer() {
        Engineer engineer = new Engineer("Анатолий", 25000);
        assertEquals("Я инженерю", engineer.engineer());
    }

    @Test
    void engineerToString() {
        Engineer engineer = new Engineer("Анатолий", 25000);
        assertEquals("Имя = Анатолий, должность = инженер, зарплата = 25000", engineer.toString());
    }
}

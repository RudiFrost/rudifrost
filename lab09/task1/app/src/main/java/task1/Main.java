package task1;

public class Main {
    public static void main(String[] args) throws MatrixLengthException {
        int n1 = 3;
        int m1 = 3;
        Matrix mat1 = new Matrix(n1, m1);
        for (int i = 0; i < mat1.getMatrix().length; i++) {
            for (int j = 0; j < mat1.getMatrix()[0].length; j++) {
                System.out.print(mat1.getMatrix()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int n2 = 3;
        int m2 = 3;
        Matrix mat2 = new Matrix(n2, m2);
        for (int i = 0; i < mat2.getMatrix().length; i++) {
            for (int j = 0; j < mat2.getMatrix()[0].length; j++) {
                System.out.print(mat2.getMatrix()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        Matrix mat3 = new Matrix(2, 2);
        int[][] array = new int[][]{{1, 2}, {3, 4}};
        mat3.setMatrix(array);
        for (int i = 0; i < mat3.getMatrix().length; i++) {
            for (int j = 0; j < mat3.getMatrix()[0].length; j++) {
                System.out.print(mat3.getMatrix()[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int[][] resultSum;
        resultSum = Operations.matrixSum(mat1, mat2);
        for (int i = 0; i < resultSum.length; i++) {
            for (int j = 0; j < resultSum[0].length; j++) {
                System.out.print(resultSum[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int[][] resMulti;
        resMulti = Operations.matrixMultiply(mat1, mat2);
        for (int i = 0; i < resMulti.length; i++) {
            for (int j = 0; j < resMulti[0].length; j++) {
                System.out.print(resMulti[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();


        int[][] resSub;
        resSub = Operations.matrixSubtraction(mat1, mat2);
        for (int i = 0; i < resSub.length; i++) {
            for (int j = 0; j < resSub[0].length; j++) {
                System.out.print(resSub[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }


}

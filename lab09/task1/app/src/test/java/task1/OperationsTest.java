package task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OperationsTest {
    @Test
    void matrixSumEquals() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(3, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        int[][] res = new int[][]{{2, 4}, {6, 8}, {10, 12}};
        assertArrayEquals(res, Operations.matrixSum(matrix1, matrix2), "work correct");
    }

    @Test
    void matrixSubtractionEquals() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(3, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        int[][] res = new int[][]{{0, 0}, {0, 0}, {0, 0}};
        assertArrayEquals(res, Operations.matrixSubtraction(matrix1, matrix2));
    }

    @Test
    void multiplyEquals() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2, 3}, {3, 4, 6}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(2, 3);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        int[][] res = new int[][]{{7, 10, 15}, {15, 22, 33}, {23, 34, 51}};
        assertArrayEquals(res, Operations.matrixMultiply(matrix1, matrix2));
    }

    @Test
    void matrixSumNullException() throws NullPointerException {
        Matrix matrix1 = new Matrix();
        Matrix matrix2 = new Matrix();
        NullPointerException exception = assertThrows(NullPointerException.class, () -> Operations.matrixSum(matrix1, matrix2));
        assertEquals("Матрицы пустые", exception.getMessage());
    }

    @Test
    void matrixSubtractionNullException() throws NullPointerException {
        Matrix matrix1 = new Matrix();
        Matrix matrix2 = new Matrix();
        NullPointerException exception = assertThrows(NullPointerException.class, () -> Operations.matrixSubtraction(matrix1, matrix2));
        assertEquals("Матрицы пустые", exception.getMessage());
    }

    @Test
    void matrixMultiplicationNullException() throws NullPointerException {
        Matrix matrix1 = new Matrix();
        Matrix matrix2 = new Matrix();
        NullPointerException exception = assertThrows(NullPointerException.class, () -> Operations.matrixMultiply(matrix1, matrix2));
        assertEquals("Матрицы пустые", exception.getMessage());
    }

    @Test
    void matrixSumLengthException() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(2, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatrixLengthException exception = assertThrows(MatrixLengthException.class, () -> Operations.matrixSum(matrix1, matrix2));
        assertEquals("Матрицы не одинаковые", exception.getMessage());
    }

    @Test
    void matrixSubtractionLengthException() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}, {3, 4}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(2, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatrixLengthException exception = assertThrows(MatrixLengthException.class, () -> Operations.matrixSubtraction(matrix1, matrix2));
        assertEquals("Матрицы не одинаковые", exception.getMessage());
    }

    @Test
    void matrixMultiplicationLengthException() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}, {5, 6}};
        int[][] m2 = new int[][]{{1, 2}};
        Matrix matrix1 = new Matrix(3, 2);
        Matrix matrix2 = new Matrix(1, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
        MatrixLengthException exception = assertThrows(MatrixLengthException.class, () -> Operations.matrixMultiply(matrix1, matrix2));
        assertEquals("Высота первой и ширина второй матриц не совпадают", exception.getMessage());
    }
}







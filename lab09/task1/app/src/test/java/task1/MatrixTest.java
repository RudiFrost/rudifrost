package task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class MatrixTest {

    @Test
    void SetterGetterMatrix() throws MatrixLengthException {
        int[][] m1 = new int[][]{{1, 2}, {3, 4}};
        Matrix matrix = new Matrix(2, 2);
        matrix.setMatrix(m1);
        assertArrayEquals(m1, matrix.getMatrix());
    }

    @Test
    void NullSetMatrix() throws MatrixLengthException {
        Matrix matrix = new Matrix();
        int[][] m1 = new int[][]{{1},{2}};
        MatrixLengthException exception = assertThrows(MatrixLengthException.class, () -> matrix.setMatrix(m1));
        assertEquals("Некорректная матрица", exception.getMessage());

    }

    @Test
    void differentSizeSetMatrix() throws MatrixLengthException {
        Matrix matrix = new Matrix(2, 2);
        int[][] m1 = new int[][]{{1, 2, 3}, {4, 5, 6}};
        MatrixLengthException exception = assertThrows(MatrixLengthException.class, () -> matrix.setMatrix(m1));
        assertEquals("Некорректная матрица", exception.getMessage());
    }
}

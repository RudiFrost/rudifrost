package task2;

public class Vector {
    private double x1;
    private double y1;
    private double x2;
    private double y2;

    public Vector(double x1, double y1, double x2, double y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Vector(Vector v) {
        this.x1 = v.getX1();
        this.x2 = v.getX2();
        this.y1 = v.getY1();
        this.y2 = v.getY2();
    }


    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public double getY2() {
        return y2;
    }

    public void setY2(double y2) {
        this.y2 = y2;
    }

    public String toString() {
        return "Начало вектора (" + this.x1 + ";" + this.y1 + ")" + "\n" +
                "Конец вектора (" + this.x2 + ";" + this.y2 + ")";
    }

    public boolean equals(Object o) {
        if (o.getClass() == this.getClass()) {
            Vector vector = (Vector) o;
            return Double.compare(vector.x1, x1) == 0 && Double.compare(vector.x2, x2) == 0 &&
                    Double.compare(vector.y1, y1) == 0 && Double.compare(vector.y2, y2) == 0;
        }
        return false;
    }

    public static Vector sum(Vector v1, Vector v2) {
        Vector newV = new Vector(v1);
        newV.sum(v2);
        return newV;
    }

    public Vector sum(Vector v1) {
        double x2v = v1.getX2() - v1.getX1();
        double y2v = v1.getY2() - v1.getY1();
        this.x2 += x2v;
        this.y2 += y2v;
        return this;
    }

    public static Vector subtraction(Vector v1, Vector v2) {
        Vector newV = new Vector(v1);
        newV.subtraction(v2);
        return newV;
    }

    public Vector subtraction(Vector v1) {
        double buffer1 = this.x1;
        double buffer2 = this.y1;
        this.x1 = this.x2 + v1.getX2() - v1.getX1();
        this.y1 = this.y2 + v1.getY2() - v1.getY1();
        this.x2 = buffer1;
        this.y2 = buffer2;
        return this;
    }

    public static Vector multiply(Vector v, double n) {
        Vector newV = new Vector(v);
        newV.multiply(n);
        return newV;
    }

    public Vector multiply(double n) {
        this.x2 *= n;
        this.y2 *= n;
        return this;
    }

    public static Vector division(Vector v, double n) {
        Vector newV = new Vector(v);
        newV.division(n);
        return newV;
    }

    public Vector division(double n) {
        this.x2 /= n;
        this.y2 /= n;
        return this;
    }
}

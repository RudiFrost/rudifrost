package task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {
    @Test
    void vectorGetter() {
        Vector v = new Vector(1, 1, 2, 2);
        assertEquals(2, v.getX2());
    }

    @Test
    void vectorToString() {
        Vector v = new Vector(1, 1, 2, 2);
        assertEquals("Начало вектора (1.0;1.0)\n" +
                "Конец вектора (2.0;2.0)", v.toString());
    }

    @Test
    void vectorEqualsTrue() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(1, 1, 2, 2);
        assertTrue(vector1.equals(vector2));
    }

    @Test
    void vectorEqualsFalse() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(1, 1, 2, 3);
        assertFalse(vector1.equals(vector2));
    }

    @Test
    void vectorSumStatic() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(2, 2, 3, 3);
        Vector vectorExpected = new Vector(1, 1, 5, 5);
        assertEquals(vectorExpected, Vector.sum(vector1, vector2));
    }

    @Test
    void vectorSum() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(2, 2, 3, 3);
        Vector vectorExpected = new Vector(1, 1, 5, 5);
        assertEquals(vectorExpected, vector1.sum(vector2));
    }

    @Test
    void vectorMultiplyStatic() {
        Vector vector = new Vector(1, 1, 2, 2);
        Vector vectorExpected = new Vector(1, 1, 6, 6);
        assertEquals(vectorExpected, Vector.multiply(vector, 3));
    }

    @Test
    void vectorMultiply() {
        Vector vector = new Vector(1, 1, 2, 2);
        Vector vectorExpected = new Vector(1, 1, 6, 6);
        assertEquals(vectorExpected, vector.multiply(3));
    }

    @Test
    void vectorSubtractStatic() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(2, 2, 3, 3);
        Vector vectorExpected = new Vector(3, 3, 1, 1);
        assertEquals(vectorExpected, Vector.subtraction(vector1, vector2));
    }

    @Test
    void vectorSubtract() {
        Vector vector1 = new Vector(1, 1, 2, 2);
        Vector vector2 = new Vector(2, 2, 3, 3);
        Vector vectorExpected = new Vector(3, 3, 1, 1);
        assertEquals(vectorExpected, vector1.subtraction(vector2));
    }

    @Test
    void vectorDivisionStatic() {
        Vector vector = new Vector(1, 1, 6, 6);
        Vector vectorExpected = new Vector(1, 1, 3, 3);
        assertEquals(vectorExpected, Vector.division(vector, 2));
    }

    @Test
    void vectorDivision() {
        Vector vector = new Vector(1, 1, 6, 6);
        Vector vectorExpected = new Vector(1, 1, 3, 3);
        assertEquals(vectorExpected, vector.division(2));
    }
}
